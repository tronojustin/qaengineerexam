*** Settings ***
Library  SeleniumLibrary
Library  String
Library  Collections
Resource  Locators.robot

*** Keywords ***
*** Variables ***


*** Test Cases ***



Scenario1
    #sort price descending
    [Documentation]    Google Test
    [Tags]       regression

    Open Browser    ${url}    chrome
    Wait Until Element Is Visible   ${dropdown}
    Click Element   ${dropdown}
    Wait Until Element Is Visible  ${priceDescending}
    Click Element   ${priceDescending}
    @{desclist} =    Create List
    ${descList1}    Set Variable    [u'$7,900,000', u'$1,500,000', u'$1,350,000', u'$950,000', u'$415,000']


    FOR     ${i}    IN RANGE  5
    ${b}=  Set Variable     ${i+1}
    ${b}      Get Text  //body/div[@id='main-wrapper']/main[1]/div[2]/div[1]/div[1]/article[1]/div[1]/div[1]/div[1]/ul[2]/li[${b}]/div[2]/p[1]/span[1]
    Append To List      ${desclist}    ${b}
    END
    Should be equal as strings    ${desclist}    ${descList1}
    Close Browser

Scenario2
    [Documentation]    Google Test
    [Tags]       regression

    Open Browser    ${url}    chrome
    Wait Until Element Is Visible   ${dropdown}
    Click Element   ${dropdown}
    Wait Until Element Is Visible  ${priceAsc}
    Click Element   ${priceAsc}
    @{asclist} =   Create List
    ${ascList2}    Set Variable    [u'$415,000', u'$950,000', u'$1,350,000', u'$1,500,000', u'$7,900,000']
    FOR     ${i}    IN RANGE  5
    ${b}=  Set Variable     ${i+2}
    ${b}      Get Text  //body/div[@id='main-wrapper']/main[1]/div[2]/div[1]/div[1]/article[1]/div[1]/div[1]/div[1]/ul[2]/li[${b}]/div[2]/p[1]/span[1]
    Append To List      ${asclist}    ${b}
    END
    Should be equal as Strings    ${asclist}    ${ascList2}

    Close Browser

Scenario3
    [Documentation]    Google Test
    [Tags]       regression

    Open Browser    ${url}    chrome
    Wait Until Element Is Visible   ${dropdown}
    Click Element   ${dropdown}
    Wait Until Element Is Visible  ${sortAlp}
    Click Element   ${sortAlp}
    @{asclist} =   Create List
    ${ascList2}    Set Variable    [u'2055 Sand Point, Discovery Bay, California 94505', u'2684 Hillcrest Drive, Cameron Park, California 95682', u'4473 Greenview Drive, El Dorado Hills, California 95762', u'5900 Samhill Mine Court, El Dorado, California 95623', u'6890 Country Side Lane, Auburn, California 95602', u'7820 Old Auburn Road, Citrus Heights, California 95610']
    FOR     ${i}    IN RANGE  6
    ${b}=  Set Variable     ${i+1}
    ${b}      Get Text  //body/div[@id='main-wrapper']/main[1]/div[2]/div[1]/div[1]/article[1]/div[1]/div[1]/div[1]/ul[2]/li[${b}]/div[2]/span[1]/a[1]
    Append To List      ${asclist}    ${b}
    END
    Should be equal as Strings    ${asclist}    ${ascList2}

    Close Browser

Scenario4
    [Documentation]    Google Test
    [Tags]       regression

    Open Browser    ${url}    chrome
    Wait Until Element Is Visible   ${dropdown}
    Click Element   ${dropdown}
    Wait Until Element Is Visible  ${sortAlp2}
    Click Element   ${sortAlp2}
    @{asclist} =   Create List
    ${ascList2}    Set Variable    [u'7820 Old Auburn Road, Citrus Heights, California 95610', u'6890 Country Side Lane, Auburn, California 95602', u'5900 Samhill Mine Court, El Dorado, California 95623', u'4473 Greenview Drive, El Dorado Hills, California 95762', u'2684 Hillcrest Drive, Cameron Park, California 95682', u'2055 Sand Point, Discovery Bay, California 94505']
    FOR     ${i}    IN RANGE  6
    ${b}=  Set Variable     ${i+1}
    ${b}      Get Text  //body/div[@id='main-wrapper']/main[1]/div[2]/div[1]/div[1]/article[1]/div[1]/div[1]/div[1]/ul[2]/li[${b}]/div[2]/span[1]/a[1]
    Append To List      ${asclist}    ${b}
    END
    Should be equal as Strings    ${asclist}    ${ascList2}

    Close Browser

Scenario5
    [Documentation]    Google Test
    [Tags]       regression
    Open Browser    ${url}    chrome
    Maximize Browser Window
    @{widthList} =    Create List
    FOR     ${i}    IN RANGE  6
    ${b}=  Set Variable     ${i+1}
    ${width}    ${height}    Get Element Size       //body/div[@id='main-wrapper']/main[1]/div[2]/div[1]/div[1]/article[1]/div[1]/div[1]/div[1]/ul[2]/li[${b}]/div[1]/a[1]/img[1]

    Append To List    ${widthList}      ${width}
    END
    log to console      ${widthList}
    Run Keyword and Ignore Error    Should be equal     ${width}    ${300}
    Close Browser

*** Keywords ***
