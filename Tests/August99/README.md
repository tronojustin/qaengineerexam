Set Up for Robot Framework-Selenium
1. Download and install python and pip from https://www.python.org/downloads/mac-osx/
    - verify if installation is successful with python --version via terminal
    - verify if installation is successful with pip --version via terminal
2. Install robot framework
    - install via terminal pip install robotframework
3. Install SeleniumLibrary 
    - install via terminal pip install robotframework-seleniumlibrary
3. Install chromedriver 
    - if homebrew is not yet existing, kindly install: 
        https://brew.sh/
    - after installation 
        brew install chromedriver     


Run automation 
1. Go to destination folder of the test script after pulling from git 
2. to run the automation script
    - run python -m robot -d Tests/August99/Results Tests/August99/Functional.robot